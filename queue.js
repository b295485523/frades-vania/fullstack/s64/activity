let collection = [];
let frontIndex = 0;

function print() {
  const elements = [];
  for (let i = frontIndex; i < collection.length; i++) {
    elements[elements.length] = collection[i];
  }
  console.log(elements);
  return elements;
}

function enqueue(element) {
  const newCollection = [];
  for (let i = frontIndex; i < collection.length; i++) {
    newCollection[newCollection.length] = collection[i];
  }
  newCollection[newCollection.length] = element;
  collection = newCollection;
  frontIndex = 0;
  return collection;array
}

function dequeue() {
  if (isEmpty()) {
    return ''; 
  }
  const removedElement = collection[frontIndex];
  frontIndex++;
  return removedElement;
}

function front() {
  if (isEmpty()) {
    return undefined; 
  }
  return collection[frontIndex];
}

function size() {
  return collection.length - frontIndex;
}

function isEmpty() {
  return frontIndex >= collection.length;
}

module.exports = {
  collection,
  print,
  enqueue,
  dequeue,
  front,
  size,
  isEmpty,
};
